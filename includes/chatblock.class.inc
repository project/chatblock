<?php

/**
 * @file
 * Drupal chatblock application class.
 */

class chatblock {

  const TIME_UNIT_YEAR = 5;
  const TIME_UNIT_MONTH = 4;
  const TIME_UNIT_WEEK = 3;
  const TIME_UNIT_DAY = 2;
  const TIME_UNIT_HOUR = 1;
  const TIME_UNIT_MINUTE = 0;

  /**
   * Callback function. Cleans up message logs.
   *
   * @param int $timerange
   *   (Optional) positive integer determining the amount of time units.
   *   Will fall back to the configured global variable, if omitted.
   * @param int $unit
   *   (Optional) one of the chatblock::TIME_UNIT_ constants.
   *   Will fall back to the configured global variable, if omitted.
   *
   * @return int|bool
   *   The result of the DB operation (number of deleted messages) on success,
   *   FALSE on failures.
   *
   * @see chatblock_cleanup_manual()
   * @see chatblock_cron()
   */
  public static function cleanup($timerange = NULL, $unit = NULL) {
    if (
      (!empty($timerange))
      ||
      ($timerange = variable_get('chatblock_autodelete_value', 0))
    ) {
      if (!isset($unit)) {
        $unit = variable_get('chatblock_autodelete_unit', chatblock::TIME_UNIT_MINUTE);
      }
      // Multiply range by days, if necessary.
      switch ($unit) {
        case chatblock::TIME_UNIT_YEAR:
          $timerange *= 365;
          break;
        case chatblock::TIME_UNIT_MONTH:
          $timerange *= 30;
          break;
        case chatblock::TIME_UNIT_WEEK:
          // Weeks
          $timerange *= 7;
          break;
      }
      // Normalize range to seconds.
      switch ($unit) {
        case chatblock::TIME_UNIT_YEAR:
        case chatblock::TIME_UNIT_MONTH:
        case chatblock::TIME_UNIT_WEEK:
        case chatblock::TIME_UNIT_DAY:
          $timerange *= 24;
        case chatblock::TIME_UNIT_HOUR:
          $timerange *= 60;
        case chatblock::TIME_UNIT_MINUTE:
          $timerange *= 60;
          break;
      }
      $dueDate = REQUEST_TIME - $timerange;
      watchdog('chatblock', 'Cleaning up messages older than !due_date from chatblock logs.', array(
        '!due_date' => format_date($dueDate)
      ));
      $dbResult = db_delete('chatblock')
        ->condition('timestamp', $dueDate, '<')
        ->execute();
      if (FALSE !== $dbResult) {
        _chatblock_database('cache_rebuild');
        watchdog('chatblock', 'Database operation successful. @count messages have been cleaned up.', array(
          '@count' => $dbResult,
        ));
      }
      else {
        watchdog('chatblock', 'A database error has occured. No messages could be deleted.', array(), WATCHDOG_ERROR);
      }
      return $dbResult;
    }
    return FALSE;
  }
}
